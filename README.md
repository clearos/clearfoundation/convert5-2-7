# convert5-2-7

This program and its scripts will help a user to construct a restoration script from a ClearOS 5 system into a ClearOS 7 system. Currently, ClearOS 6 can be restored to ClearOS 7.

# Installation
You can grab the sources from this site. To prepare, ensure that you have ClearOS installed and then make sure to install 'git'.

`yum install git`



